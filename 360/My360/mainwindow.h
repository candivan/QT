#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <closebutton.h>
#include <titlebar.h>
#include <QPropertyAnimation>
#include <QtWidgets/QSystemTrayIcon>
#include <centerwidget.h>
#include <basewidget.h>
#include <memory>
#include <iostream>

class QStateMachine;
class QState;
class QSignalTransition;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    static MainWindow* getInstance()
    {
        return newInstance();
    }

    static void  deleteSth(void)
    {
          std::cout << "in MainWindow::deleteSth();" << std::endl;
             //if (instance != NULL)
                //delete instance;
    }


private:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();
    static MainWindow* instance;
    static MainWindow* newInstance(void)
    {
        if(instance == NULL){
            instance = new MainWindow;
        }

        return instance;

    }

public slots:
    void unFix();

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);

private:
    CenterWidget* center;

    void InitUi();
    void InitConnect();
    void InitAnimation();

    bool isMousePress;
    QPoint windowPos;
    QPoint mousePos;

    QPropertyAnimation*  closeOpacityAnimation;
    QPropertyAnimation* closemoveAnimation;
    QSystemTrayIcon* Tray;
    QRect WindowGeometry;

    QStateMachine *closeMachine;
    QState *start;
    QState *end;
    QSignalTransition *tran ;
    QStateMachine *moveMachine ;
    QState *moveStart;
    QState *moveEnd  ;
    QSignalTransition *moveTran;
    Q_DISABLE_COPY(MainWindow)
};


#endif // MAINWINDOW_H
